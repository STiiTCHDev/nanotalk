import { Body, Controller, Get, Post } from '@nestjs/common';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {

    constructor(private readonly usersService: UsersService) {}

    @Get()
    public async getUsers() {
        const users = this.usersService.getUsers();
        return users.map(user => ({ id: user.id, username: user.username }));
    }

    @Post()
    public async addUser(@Body() body: { username: string }) {
        const user = await this.usersService.addUser(body.username);
        return { id: user.id, username: user.username };
    }
}
