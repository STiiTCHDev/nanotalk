import { Injectable } from '@nestjs/common';
import * as crypto from 'crypto';

@Injectable()
export class UsersService {
    private users = new Map<string, string>();

    public async addUser(username: string) {
        const id = crypto.randomUUID();
        this.users.set(id, username);
        console.log(`Added user ${username} with id ${id}`);
        return { id, username };
    }

    public getUsers() {
        return Array.from(this.users.entries()).map(([id, username]) => ({ id, username }));
    }

    public getUser(id: string) {
        return this.users.get(id);
    }
}
